
package noobbot;

import java.io.IOException;

import bot.Bot;

public class Main
{
  public static void main( final String... args ) throws IOException, NumberFormatException
  {
    new Bot().run( args[ 0 ], Integer.parseInt( args[ 1 ] ), args[ 2 ], args[ 3 ] );
  }
}
