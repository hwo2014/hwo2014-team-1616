package bot;

import java.util.ArrayList;
import java.util.List;

import msg.CarData;
import msg.CarPosition;
import msg.MsgCarPositions;
import msg.MsgCrash;
import msg.MsgGameInit;
import msg.MsgSpawn;
import msg.MsgThrottle;
import msg.MsgType;
import msg.MsgYourCar;
import msg.Piece;
import msg.PiecePosition;
import msg.Track;

import com.google.gson.Gson;

public final class Tracker
{
  private final Gson    gson          = U.gson;

  public String         car;

  public Track          track;
  public CarData[]      cars;

  public boolean        crashed;
  public CarPosition[]  carPositions;
  public CarPosition    carPosition;
  public Piece          piece;
  public PiecePosition  piecePosition;
  public Piece          prevPiece;
  public PiecePosition  prevPiecePosition;

  public double         throttle;
  public int            lastCrashTick = -1;

  public DataBuffer     dataBuf       = new DataBuffer();

  public final int[]    dataMapper;
  public final int[]    targetMapper;

  public List<double[]> data          = new ArrayList<>();
  public List<double[]> target        = new ArrayList<>();

  public Tracker( final int[] dataMapper1, final int[] targetMapper1 )
  {
    dataMapper = dataMapper1;
    targetMapper = targetMapper1;
  }

  public CarData getCarData()
  {
    for( final CarData cd : cars )
    {
      if( car.equals( cd.getId().getName() ) )
        return cd;
    }

    return null;
  }

  private double getCarVelocity()
  {
    final double v;

    if( prevPiece == null )
    {
      v = 0;
    }
    else
    {
      if( prevPiece == piece )
      {
        v = piecePosition.getInPieceDistance() - prevPiecePosition.getInPieceDistance();
      }
      else
      {
        v = piecePosition.getInPieceDistance()
                  + U.getLaneLength( track, prevPiece, prevPiecePosition.getLane().getStartLaneIndex() )
                  - prevPiecePosition.getInPieceDistance();
      }
    }

    return v;
  }

  public void track()
  {
    dataBuf.track
    ( getCarVelocity(),
      carPosition.getAngleInRadians(),
      U.getLaneCurvature( track, piece, piecePosition.getLane().getStartLaneIndex() ),
      throttle
    );
  }

  public void record()
  {
    if( dataMapper != null )
      dataBuf.record();
  }

  private void extract()
  {
    if( dataMapper != null )
    {
      data.addAll( dataBuf.getData( dataMapper ) );
      target.addAll( dataBuf.getTarget( targetMapper ) );
    }

    dataBuf.reset();
  }

  public void yourCar( final MsgYourCar m )
  {
    car = m.getData().getName();
  }

  public void gameInit( final MsgGameInit m )
  {
    track = m.getData().getRace().getTrack();
    cars = m.getData().getRace().getCars();
    throttle = 0;
    dataBuf.reset();
  }

  public boolean carPosition( final MsgCarPositions m )
  {
    if( crashed && lastCrashTick != m.getGameTick() )
      return false;

    carPositions = m.getData();

    for( final CarPosition cp : carPositions )
    {
      if( car.equals( cp.getId().getName() ) )
      {
        carPosition = cp;

        prevPiece = piece;
        prevPiecePosition = piecePosition;
        piecePosition = cp.getPiecePosition();
        piece = track.getPieces()[ piecePosition.getPieceIndex() ];
        break;
      }
    }

    return true;
  }

  public void readThrottle( final MsgThrottle m )
  {
    if( crashed ) return;
    setThrottle( m.getData() );
  }

  public void setThrottle( final double newThrottle )
  {
    throttle = newThrottle;
    dataBuf.setThrottle( newThrottle );
    record();
  }

  public void turbo()
  {
    if( crashed ) return;
    record();
  }

  public void switchLane()
  {
    if( crashed ) return;
    record();
  }

  public void crash( final MsgCrash m )
  {
    if( car.equals( m.getData().getName() ) )
    {
      lastCrashTick = m.getGameTick();
      crashed = true;
      throttle = 0;
      extract();
    }
  }

  public void spawn( final MsgSpawn m )
  {
    if( car.equals( m.getData().getName() ) )
    {
      crashed = false;
      throttle = 0;
    }
  }

  public void gameEnd()
  {
    track = null;
    cars = null;
    throttle = 0;

    prevPiece = null;
    prevPiecePosition = null;
    piece = null;
    piecePosition = null;

    extract();
  }

  public void track( final MsgType t, final String line )
  {
    switch( t )
    {
      case YOUR_CAR:
        yourCar( gson.fromJson( line, MsgYourCar.class ) );
        break;
      case GAME_INIT:
        gameInit( gson.fromJson( line, MsgGameInit.class ) );
        break;
      case CAR_POSITIONS:
        if( carPosition( gson.fromJson( line, MsgCarPositions.class ) ) )
          track();
        break;
      case THROTTLE:
        readThrottle( gson.fromJson( line, MsgThrottle.class ) );
        break;
      case TURBO:
        turbo();
        break;
      case SWITCH_LANE:
        switchLane();
        break;
      case CRASH:
        crash( gson.fromJson( line, MsgCrash.class ) );
        break;
      case SPAWN:
        spawn( gson.fromJson( line, MsgSpawn.class ) );
        break;
      case GAME_END:
        gameEnd();
        break;
      default:
        break;
    }
  }
}
