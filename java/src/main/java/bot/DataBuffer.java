package bot;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class DataBuffer
{
  public static final int      BUF_INPUT_SIZE            = 4;
  public static final int      TICKS_IN_BUFFER           = 1000;

  public static final int      SIZE                      = 16;

  public static final double   MAX_A                     = 60 * Math.PI / 180;

  public static final int      IX_V0                     = 0;
  public static final int      IX_V1                     = 1;
  public static final int      IX_V2                     = 2;
  public static final int      IX_V3                     = 3;
  public static final int      IX_A0                     = 4;
  public static final int      IX_A1                     = 5;
  public static final int      IX_A2                     = 6;
  public static final int      IX_A3                     = 7;
  public static final int      IX_CURV0                  = 8;
  public static final int      IX_CURV1                  = 9;
  public static final int      IX_CURV2                  = 10;
  public static final int      IX_CURV3                  = 11;
  public static final int      IX_THR0                   = 12;
  public static final int      IX_THR1                   = 13;
  public static final int      IX_THR2                   = 14;
  public static final int      IX_THR3                   = 15;

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  public static final int[] DATA = new int[]
  { IX_V0, IX_V1, IX_V2,
    IX_A0, IX_A1, IX_A2,
    IX_CURV0, IX_CURV1, IX_CURV2,
    IX_THR0, IX_THR1, IX_THR2 };

  public static final int[] TARGET = new int[] { IX_V0, IX_A0 };
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

  // output
  public final double[]        output                    = new double[ SIZE ];

  private final double[][]     buf                       = new double[ BUF_INPUT_SIZE ][ TICKS_IN_BUFFER ];
  private int                  bufPos                    = 0;

  private final double[]       bufV                      = buf[ 0 ];
  private final double[]       bufA                      = buf[ 1 ];
  private final double[]       bufCURV                   = buf[ 2 ];
  private final double[]       bufTHR                    = buf[ 3 ];

  public final double[]        savedOutput               = new double[ SIZE ];
  public int                   savedBufPos               = 0;

  private final List<double[]> data                      = new ArrayList<>();

  public void reset()
  {
    for( final double[] var : buf )
      Arrays.fill( var, 0 );

    bufPos = 0;
    data.clear();

    Arrays.fill( output, 0 );
  }

  public void savePos()
  {
    savedBufPos = bufPos;
  }

  public void saveOutput()
  {
    System.arraycopy( output, 0, savedOutput, 0, SIZE );
  }

  public void restorePos()
  {
    bufPos = savedBufPos;
  }

  public void restoreOutput()
  {
    System.arraycopy( savedOutput, 0, output, 0, SIZE );
  }

  private void emit()
  {
    final int n1 = ( bufPos + TICKS_IN_BUFFER - 1 ) % TICKS_IN_BUFFER;
    final int n2 = ( bufPos + TICKS_IN_BUFFER - 2 ) % TICKS_IN_BUFFER;
    final int n3 = ( bufPos + TICKS_IN_BUFFER - 3 ) % TICKS_IN_BUFFER;

    output[ IX_V0      ] = bufV[ bufPos ];
    output[ IX_V1      ] = bufV[ n1 ];
    output[ IX_V2      ] = bufV[ n2 ];
    output[ IX_V3      ] = bufV[ n3 ];

    output[ IX_A0      ] = bufA[ bufPos ];
    output[ IX_A1      ] = bufA[ n1 ];
    output[ IX_A2      ] = bufA[ n2 ];
    output[ IX_A3      ] = bufA[ n3 ];

    output[ IX_CURV0   ] = bufCURV[ bufPos ];
    output[ IX_CURV1   ] = bufCURV[ n1 ];
    output[ IX_CURV2   ] = bufCURV[ n2 ];
    output[ IX_CURV3   ] = bufCURV[ n3 ];

    output[ IX_THR0    ] = bufTHR[ bufPos ];
    output[ IX_THR1    ] = bufTHR[ n1 ];
    output[ IX_THR2    ] = bufTHR[ n2 ];
    output[ IX_THR3    ] = bufTHR[ n3 ];

    bufPos = ( bufPos + 1 ) % TICKS_IN_BUFFER;
  }

  public double getV( final int shift )
  {
    return bufV[ ( bufPos + TICKS_IN_BUFFER - 1 - shift ) % TICKS_IN_BUFFER ];
  }

  public void track( final double v, final double a, final double curv, final double throttle )
  {
    bufV[ bufPos ] = v;
    bufA[ bufPos ] = a;
    bufCURV[ bufPos ] = curv;
    bufTHR[ bufPos ] = throttle;

    emit();
  }

  public void backtrack( final int ticks )
  {
    bufPos = ( bufPos + TICKS_IN_BUFFER - ticks - 1 ) % TICKS_IN_BUFFER;
    emit();
  }

  public void setThrottle( final double throttle )
  {
    output[ IX_THR0 ] = throttle;
    bufTHR[ ( bufPos + TICKS_IN_BUFFER - 1 ) % TICKS_IN_BUFFER ] = throttle;
  }

  public void record()
  {
    data.add( Arrays.copyOf( output, SIZE ) );
  }

  public List<double[]> getData( final int[] mapper )
  {
    final List<double[]> result = new ArrayList<>( data.size() );

    for( int i = 0; i < data.size() - 1; i++ )
    {
      result.add( U.map( mapper, data.get( i ) ) );
    }

    return result;
  }

  public List<double[]> getTarget( final int[] mapper )
  {
    final List<double[]> target = new ArrayList<>( data.size() );

    for( int i = 1; i < data.size(); i++ )
    {
      target.add( U.map( mapper, data.get( i ) ) );
    }

    return target;
  }
}
