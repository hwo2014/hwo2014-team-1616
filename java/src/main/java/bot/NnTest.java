package bot;

import static java.lang.Math.PI;
import static java.lang.Math.cos;
import static java.lang.Math.sin;

import java.io.IOException;
import java.io.PrintStream;
import java.nio.file.Files;

import nn.F;
import nn.NnInstance;

@SuppressWarnings( { "boxing", "nls" } )
public class NnTest
{
  public static void main( final String[] args ) throws IOException
  {
    U.init();
    U.rng.setSeed( 834723987445377L );

    final int K = 100;
    final int N = K * K;

    final double[] xy = new double[ K ];

    for( int i = 0; i < K; i++ )
    {
      xy[ i ] = i / (double)K - 0.5;
    }

    final double[][] data = new double[ N ][];
    final double[][] target = new double[ N ][];

    for( int i = 0; i < K; i++ )
    {
      for( int j = 0; j < K; j++ )
      {
        final double f = sin( 2 * PI * xy[ i ] ) * cos( 2* PI * xy[ j ] );
        data[ i*K + j ] = new double[] { xy[ i ], xy[ j ] };
        target[ i*K + j ] = new double[] { f };
      }
    }

    final NnInstance nn = new NnInstance( "test", F.SQUARE, F.SIGMOID_XdivSqrt1pX2, 2, 25, 2, 1 );
    nn.prepare( data, target );
    nn.learn( data, target, 10000 );

//    final NnInstance nn = NnInstance.load( U.getNnPath( "nn-test.txt" ) );

    final double[] prediction = new double[ 1 ];

    try( PrintStream ps = new PrintStream(
        Files.newOutputStream( U.RACE_LOGS.resolve( "xy.txt" ) ) ) )
    {
      for( int i = 0; i < K; i++ )
        ps.printf( U.DOUBLE_F_FORMAT + "\n", xy[ i ] );
    }

    try( PrintStream psT = new PrintStream(
           Files.newOutputStream( U.RACE_LOGS.resolve( "t.txt" ) ) );
         PrintStream psP = new PrintStream(
           Files.newOutputStream( U.RACE_LOGS.resolve( "p.txt" ) ) );
         PrintStream psD = new PrintStream(
           Files.newOutputStream( U.RACE_LOGS.resolve( "d.txt" ) ) ) )
    {
      for( int i = 0; i < K; i++ )
      {
        for( int j = 0; j < K; j++ )
        {
          final int ij = i*K + j;
          nn.predict( data[ ij ], prediction );

          psT.printf( U.DOUBLE_F_FORMAT, target[ ij ][ 0 ] );
          psP.printf( U.DOUBLE_F_FORMAT, prediction[ 0 ] );
          psD.printf( U.DOUBLE_F_FORMAT, target[ ij ][ 0 ] - prediction[ 0 ] );
        }

        psT.println();
        psP.println();
        psD.println();
      }
    }
  }
}
