package bot;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;

import msg.MsgCarPositions;
import msg.MsgCrash;
import msg.MsgCreateRace;
import msg.MsgGameEnd;
import msg.MsgGameInit;
import msg.MsgJoin;
import msg.MsgJoinRace;
import msg.MsgLapFinished;
import msg.MsgPing;
import msg.MsgSpawn;
import msg.MsgSwitchLane;
import msg.MsgThrottle;
import msg.MsgTurbo;
import msg.MsgTurboAvailable;
import msg.MsgTurboEnd;
import msg.MsgTurboStart;
import msg.MsgType;
import msg.MsgYourCar;

import com.google.gson.Gson;

@SuppressWarnings( { "nls", "boxing" } )
public final class Bot
{
  final Gson     gson   = U.gson;
  final Driver   driver = new Driver();

  BufferedReader reader;
  PrintWriter    writer;
  List<String>   lines  = new ArrayList<>( 50000 );

  public void replay( final String raceFile ) throws UnknownHostException, IOException
  {
    U.init();
    U.replay = true;
    U.log( "Replaying file " + raceFile + "\n" );

    writer = new PrintWriter( Files.newBufferedWriter( U.RACE_LOGS.resolve( "replay.log" ) ) );
    reader = Files.newBufferedReader( U.RACE_REPO.resolve( raceFile ) );

    communicateReplay();
  }

  private void communicateReplay() throws IOException
  {
    lines.clear();
    String line = null;

    while( ( line = reader.readLine() ) != null )
    {
      final int pos = line.indexOf( ' ' );
      final String msg = line.substring( pos + 1 );
      final MsgType t = MsgType.get( msg );

      if( t == null )
      {
        // log( "Unknown message type: %s", msg );
        saveLine( msg );
        continue;
      }

      dispatch( t, msg );
    }
  }

  public void run
  ( final String realHost, final int port, final String botName, final String botKey )
  throws UnknownHostException, IOException
  {
    U.init();

    String host = realHost;
    String track = null;
    String passwd = null;
    int carCount = 1;
    boolean newRace = false;

    String extendedTest = System.getenv( "HWO_EXT_TEST" );

    if( extendedTest  == null )
      extendedTest = System.getProperty( "HWO_EXT_TEST" );

    if( extendedTest != null )
    {
      final String[] data = extendedTest.split( ":" );

      host = data[ 0 ];
      track = data[ 1 ];
      passwd = data[ 2 ].isEmpty() ? null : data[ 2 ];
      carCount = Integer.parseInt( data[ 3 ] );
      newRace = "create".equals( data[ 4 ] );
    }

    U.log( "Connecting to " + host + ":" + port + " as " + botName + "\n" );

    try ( Socket socket = new Socket( host, port ) )
    {
      writer = new PrintWriter( new OutputStreamWriter( socket.getOutputStream(), "utf-8" ) );
      reader = new BufferedReader( new InputStreamReader( socket.getInputStream(), "utf-8" ) );

      if( extendedTest != null )
      {
        if( newRace )
          send( new MsgCreateRace( botName, botKey, track, passwd, carCount ) );
        else
          send( new MsgJoinRace( botName, botKey, track, passwd, carCount ) );
      }
      else
      {
        send( new MsgJoin( botName, botKey ) );
      }

      communicate();
    }

    saveToRepo( true );
  }

  private void communicate() throws IOException
  {
    lines.clear();
    String line = null;

    while( ( line = reader.readLine() ) != null )
    {
      final MsgType t = MsgType.get( line );

      if( t == null )
      {
        log( "Unknown message type: %s", line );
        saveLine( line );
        continue;
      }

      dispatch( t, line );

      if( driver.lastGameTick % 600 == 0 && driver.lastGameTick > 0 )
        saveToRepo( false );
    }
  }

  private void dispatch( final MsgType t, final String line )
  {
    switch( t )
    {
      case FAILURE:
        saveLine( line );
        log( "%s: %s", t.type(), line );
        break;
      case JOIN:
        driver.join( gson.fromJson( line, MsgJoin.class ) );
        saveLine( line );
        log( "%s: %s", t.type(), line );
        break;
      case CREATE_RACE:
        driver.createRace( gson.fromJson( line, MsgCreateRace.class ) );
        saveLine( line );
        log( "%s: %s", t.type(), line );
        break;
      case JOIN_RACE:
        driver.joinRace( gson.fromJson( line, MsgJoinRace.class ) );
        saveLine( line );
        log( "%s: %s", t.type(), line );
        break;
      case YOUR_CAR:
        driver.yourCar( gson.fromJson( line, MsgYourCar.class ) );
        saveLine( line );
        log( "%s: %s", t.type(), line );
        break;
      case GAME_INIT:
        driver.gameInit( gson.fromJson( line, MsgGameInit.class ) );
        saveLine( line );
        log( "%s: %s", t.type(), line );
        break;
      case GAME_START:
        saveLine( line );
        send( new MsgPing() );
        log( "%s: %s", t.type(), line );
        break;
      case TURBO_AVAILABLE:
        driver.turboAvailable( gson.fromJson( line, MsgTurboAvailable.class ) );
        saveLine( line );
        log( "%s: %s", t.type(), line );
        break;
      case TURBO_START:
        driver.turboStart( gson.fromJson( line, MsgTurboStart.class ) );
        saveLine( line );
        log( "%s: %s", t.type(), line );
        break;
      case TURBO_END:
        driver.turboEnd( gson.fromJson( line, MsgTurboEnd.class ) );
        saveLine( line );
        log( "%s: %s", t.type(), line );
        break;
      case CAR_POSITIONS:
        driver.carPosition( gson.fromJson( line, MsgCarPositions.class ) );
        saveLine( line );
        if( driver.crashed )
        {
          send( new MsgPing() );
          break;
        }
        final String laneSwitchAction = driver.mustSwitchLaneTo();
        if( laneSwitchAction != null )
        {
          send( new MsgSwitchLane( laneSwitchAction ) );
          log( "%s: %s", MsgType.SWITCH_LANE.type(), laneSwitchAction );
          break;
        }
        if( driver.mustUseTurbo() )
        {
          send( new MsgTurbo( "Aaaaaaaah!..." ) );
          log( "%s:...", MsgType.TURBO.type() );
          break;
        }
        send( new MsgThrottle( driver.mustThrottle() ) );
        break;
      case CRASH:
        final MsgCrash crash = gson.fromJson( line, MsgCrash.class );
        driver.crash( crash );
        saveLine( line );
        log( "%s: %s", t.type(), crash.getData().getName() );
        break;
      case SPAWN:
        final MsgSpawn spawn = gson.fromJson( line, MsgSpawn.class );
        driver.spawn( spawn );
        saveLine( line );
        log( "%s: %s", t.type(), spawn.getData().getName() );
        break;
      case LAP_FINISHED:
        final MsgLapFinished fin = gson.fromJson( line, MsgLapFinished.class );
        saveLine( line );
        if( fin.getData().getCar().getName().equals( driver.car.getName() ) )
          log( "%s: %f s", t.type(), fin.getData().getLapSeconds() );
        break;
      case FINISH:
        saveLine( line );
        log( "%s: %s", t.type(), line );
        break;
      case GAME_END:
        driver.gameEnd( gson.fromJson( line, MsgGameEnd.class ) );
        saveLine( line );
        log( "%s: %s", t.type(), line );
        break;
      case TOURNAMENT_END:
        saveLine( line );
        log( "%s: %s", t.type(), line );
        break;
      default:
        saveLine( line );
        if( !U.replay ) log( "Unhandled message %s: %s", t.type(), line );
        break;
    }
  }

  public void log( final String msg, final Object... args )
  {
    System.out.println( String.format( "%05d ", driver.lastGameTick ) + " " + String.format( msg, args ) );
  }

  private void send( final Object msg )
  {
    final String sMsg = gson.toJson( msg );
    writer.println( sMsg );
    writer.flush();
    saveLine( sMsg );
  }

  private void saveLine( final String line )
  {
    if( U.REMOTE_RUN )
      lines.add( String.format( "%05d %s", driver.lastGameTick, line ) );
  }

  private void saveToRepo( final boolean complete ) throws IOException
  {
    if( U.REMOTE_RUN && !U.replay )
    {
      final String fileName = String.format( complete ? "r-%s-%s-%d-%s.txt" : "incomplete-%s-%s-%d-%s.txt",
          U.timestamp(),
          driver.bot.getName(),
          driver.race.getCars().length,
          driver.race.getTrack().getId() );

      try( BufferedWriter bw = Files.newBufferedWriter( U.RACE_REPO.resolve( fileName ) ) )
      {
        for( final String line : lines )
        {
          bw.write( line );
          bw.newLine();
        }
      }
    }
  }

  public static void main( final String... args ) throws IOException, NumberFormatException
  {
    new Bot().replay( "r-20140528-110708-FF0-1-keimola.txt" );
  }
}
