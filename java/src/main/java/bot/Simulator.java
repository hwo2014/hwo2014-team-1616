package bot;

import static java.lang.Math.abs;
import static java.lang.Math.round;
import msg.Piece;
import msg.Track;

public final class Simulator
{
  private Track    track;
  private Piece[]  pieces;
  private int      numberOfpieces;

  private int      tickPlusOne;
  private Piece[]  piece;
  private int[]    pieceIndex;
  private int[]    lane;
  private double[] posInPiece;

  private int      ticksTotal;
  private int      ticksLeft;
  private double   thr1;
  private double   thr2;
  private double   thrDev;

  public void init( final int ticks, final Track t, final int pIndex, final int lIndex, final double pos )
  {
    tickPlusOne = 0;
    track = t;
    pieces = track.getPieces();
    numberOfpieces = pieces.length;
    pieceIndex = new int[ ticks + 1 ];
    pieceIndex[ 0 ] = pIndex;
    piece = new Piece[ ticks + 1 ];
    piece[ 0 ] = pieces[ pIndex ];
    lane = new int[ ticks + 1 ];
    lane[ 0 ] = lIndex;
    posInPiece = new double[ ticks + 1 ];
    posInPiece[ 0 ] = pos;
  }

  public void track( final double v )
  {
    final double len = U.getLaneLength( track, piece[ tickPlusOne ], lane[ tickPlusOne ] );

    if( ( posInPiece[ tickPlusOne + 1 ] = posInPiece[ tickPlusOne ] + v ) > len )
    {
      posInPiece[ tickPlusOne + 1 ] -= len;
      pieceIndex[ tickPlusOne + 1 ] = ( pieceIndex[ tickPlusOne ] + 1 ) % numberOfpieces;
      piece[ tickPlusOne + 1 ] = pieces[ pieceIndex[ tickPlusOne + 1 ] ];
    }
    else
    {
      pieceIndex[ tickPlusOne + 1 ] = pieceIndex[ tickPlusOne ];
      piece[ tickPlusOne + 1 ] = piece[ tickPlusOne ];
    }

    tickPlusOne = tickPlusOne + 1;
  }

  public void backtrack( final int ticks )
  {
    tickPlusOne -= ticks;
  }

  public double getCurvature()
  {
    return U.getLaneCurvature( track, piece[ tickPlusOne ], lane[ tickPlusOne ] );
  }

  public double randomThrottle()
  {
    if( ticksLeft == 0 )
    {
      thr1 = thr2;
      thr2  = U.rng.nextDouble() * 2.5 - 0.5;
      thrDev = U.rng.nextDouble() / 10;

      ticksTotal = 2 + (int)round( abs( U.rng.nextGaussian() * 60 ) );
      ticksLeft = ticksTotal + 1;
    }

    ticksLeft--;

    return ( thr1 * ticksLeft + thr2 * ( ticksTotal - ticksLeft ) + U.rng.nextGaussian() * thrDev ) / ticksTotal;
  }
}
