package bot;

import static java.lang.Math.max;
import static java.lang.Math.min;

import java.net.URISyntaxException;
import java.util.Arrays;

import msg.BotData;
import msg.Car;
import msg.MsgCarPositions;
import msg.MsgCrash;
import msg.MsgCreateRace;
import msg.MsgGameEnd;
import msg.MsgGameInit;
import msg.MsgJoin;
import msg.MsgJoinRace;
import msg.MsgSpawn;
import msg.MsgTurboAvailable;
import msg.MsgTurboEnd;
import msg.MsgTurboStart;
import msg.MsgYourCar;
import msg.PiecePosition;
import msg.Race;
import msg.TurboData;
import nn.NnInstance;

@SuppressWarnings( { "nls", "boxing" } )
public final class Driver
{
  public BotData          bot;
  public Car              car;
  public Race             race;
  public int              lastGameTick          = -1;
  public TurboData        turbo;
  public boolean          turboActive;
  public boolean          crashed;

  public Tracker          tracker;
  public DataBuffer       dataBuf;
  final double[]          trOut;

  static double           MAX_CRASH_CHANCE      = 0.9;
  static int              MAX_TICKS_TO_SIMULATE = 60;
  static int              THROTTLE_STEPS        = 20;

  static double[]         THROTTLE_SINGLE_VALUE = new double[ 1 ];
  static double[]         THROTTLE_VALUES_ACTIVE = new double[ THROTTLE_STEPS + 1 ];
//  static double[]         THROTTLE_VALUES_PASSIVE = new double[ THROTTLE_STEPS + 1 ];

  static
  {
    THROTTLE_VALUES_ACTIVE[ 0 ] = 1;
    THROTTLE_VALUES_ACTIVE[ THROTTLE_STEPS ] = 0;
//    THROTTLE_VALUES_PASSIVE[ 0 ] = 0;
//    THROTTLE_VALUES_PASSIVE[ THROTTLE_STEPS ] = 1;

    for( int i = 1; i < THROTTLE_STEPS; i++ )
    {
      final double thr = 1. * i / THROTTLE_STEPS;
      THROTTLE_VALUES_ACTIVE[ THROTTLE_STEPS - i ] = thr;
//      THROTTLE_VALUES_PASSIVE[ i ] = thr;
    }
  }

  double[]                plan                   = new double[ MAX_TICKS_TO_SIMULATE ];
  int                     tick;
  double                  bestThr;
  double                  bestCtc;
  double                  bestV;
  double                  bestA;

  public final NnInstance nnVp;

  public final double[]   nnVpInput             = new double[ DataBuffer.DATA.length ];
  public final double[]   nnVpOutput            = new double[ DataBuffer.TARGET.length ];
  public final double[]   nnVpBackPropIn        = new double[ DataBuffer.TARGET.length ];

  private Simulator       simulator             = new Simulator();

  public Driver()
  {
    tracker = new Tracker( null, null );
    dataBuf = tracker.dataBuf;
    trOut = dataBuf.output;

    try
    {
      nnVp = NnInstance.load( U.getNnPath( "nn-vp.txt" ) );
    }
    catch( final URISyntaxException e )
    {
      e.printStackTrace();
      U.fail( e.getMessage() );
      throw new RuntimeException( e );
    }
  }

  public void join( final MsgJoin m )
  {
    bot = m.getData();
  }

  public void createRace( final MsgCreateRace m )
  {
    bot = m.getData().getBotId();
  }

  public void joinRace( final MsgJoinRace m )
  {
    bot = m.getData().getBotId();
  }

  public void yourCar( final MsgYourCar m )
  {
    car = m.getData();
    tracker.yourCar( m );
  }

  public void gameInit( final MsgGameInit m )
  {
    tracker.gameInit( m );

    race = m.getData().getRace();
    turbo = null;
    turboActive = false;
    crashed = false;
  }

  public void turboAvailable( final MsgTurboAvailable m )
  {
    lastGameTick = m.getGameTick();
    turbo = m.getData();
    turboActive = false;
  }

  public void turboStart( final MsgTurboStart m )
  {
    lastGameTick = m.getGameTick();

    if( car.getName().equals( m.getData().getName() ) )
    {
      turboActive = true;
      turbo = null;
    }
  }

  public void turboEnd( final MsgTurboEnd m )
  {
    lastGameTick = m.getGameTick();

    if( car.getName().equals( m.getData().getName() ) )
    {
      turboActive = false;
      turbo = null;
    }
  }

  public void carPosition( final MsgCarPositions m )
  {
    lastGameTick = m.getGameTick();

    if( tracker.carPosition( m ) )
    {
      tracker.track();
    }
  }

  public void crash( final MsgCrash m )
  {
    lastGameTick = m.getGameTick();
    tracker.crash( m );

    if( car.getName().equals( m.getData().getName() ) )
    {
      crashed = true;
      turboActive = false;
      turbo = null;
    }
  }

  public void spawn( final MsgSpawn m )
  {
    lastGameTick = m.getGameTick();
    tracker.spawn( m );

    if( car.getName().equals( m.getData().getName() ) )
    {
      crashed = false;
      turboActive = false;
      turbo = null;
    }
  }

  public void gameEnd( @SuppressWarnings( "unused" ) final MsgGameEnd m )
  {
    tracker.gameEnd();
  }

  @SuppressWarnings( "static-method" )
  public boolean mustUseTurbo()
  {
    return false;
  }

  @SuppressWarnings( "static-method" )
  public String mustSwitchLaneTo()
  {
    // "Left" "Right"
    return null;
  }

  public double mustThrottle()
  {
//    final double throttle = simulator.randomThrottle();
    final double throttle = optimalThrottle();

    if( U.replay )
      U.log( "%05d   dist: %f   v: %f   a: %f   r: %d   "
           + "thr: %f",
             lastGameTick,
             tracker.piecePosition.getInPieceDistance(),
             trOut[ DataBuffer.IX_V0 ],
             tracker.carPosition.getAngleInRadians(),
             tracker.piece.getRadius(),
             throttle );

    final double checkedThrotle = min( 1, max( 0, throttle ) );
    tracker.setThrottle( checkedThrotle );
    return checkedThrotle;
  }

  private double optimalThrottle()
  {
    dataBuf.saveOutput();
    dataBuf.savePos();

    final PiecePosition piecePos = tracker.carPosition.getPiecePosition();
    final int pieceIndex = piecePos.getPieceIndex();
    final int lane = piecePos.getLane().getStartLaneIndex();
    final double pos = piecePos.getInPieceDistance();
    simulator.init( MAX_TICKS_TO_SIMULATE, race.getTrack(), pieceIndex, lane, pos );
    Arrays.fill( plan, -1 );

    for( tick = 0; tick < MAX_TICKS_TO_SIMULATE; /*nothing */ )
    {
      if( stepForward() )
        continue;

      if( tick == 0 )
        break;

      stepBackward();

      if( plan[ 0 ] == 0 )
        break;
    }

    dataBuf.restorePos();
    dataBuf.restoreOutput();
    return plan[ 0 ];
  }

  private void stepBackward()
  {
    double thr, delta;

    while( tick > 0 )
    {
      dataBuf.backtrack( 1 );
      simulator.backtrack( 1 );
      tick--;

      delta = 0.1;
      thr = Math.max( Math.min( plan[ tick ] - delta, 1 ), 0 );

      if( Math.abs( thr - plan[ tick ] ) > 0 && ( tick == 0 || plan[ tick - 1 ] - thr  <= delta * 1.2 ) )
      {
        plan[ tick ] = thr;
        return;
      }
    }
  }

  private boolean stepForward()
  {
    bestThr = 1;
    bestCtc = 1000;
    bestV = -1000;
    bestA = -1000;

    THROTTLE_SINGLE_VALUE[ 0 ] = plan[ tick ];

    for( final double thr : plan[ tick ] == -1 ? THROTTLE_VALUES_ACTIVE : THROTTLE_SINGLE_VALUE )
    {
      trOut[ DataBuffer.IX_THR0 ] = thr;
      U.map( DataBuffer.DATA, trOut, nnVpInput );
      nnVp.predict( nnVpInput, nnVpOutput );
      final double ctc = Math.abs( nnVpOutput[ 1 ] );

      if( ctc <= MAX_CRASH_CHANCE )
      {
        bestThr = thr;
        bestCtc = ctc;
        bestV = nnVpOutput[ 0 ];
        bestA = nnVpOutput[ 1 ];
        break;
      }

      if( ctc <= bestCtc )
      {
        bestThr = thr;
        bestCtc = ctc;
        bestV = nnVpOutput[ 0 ];
        bestA = nnVpOutput[ 1 ];
      }
    }

    if( bestCtc <= MAX_CRASH_CHANCE )
    {
      plan[ tick ] = bestThr;
      simulator.track( bestV );
      dataBuf.track( bestV, bestA, simulator.getCurvature(), bestThr );
      tick++;
      return true;
    }

    return false;
  }

  @Override
  public String toString()
  {
    final StringBuilder sb = new StringBuilder();

    for( int i = 0; i < MAX_TICKS_TO_SIMULATE; i++ )
    {
      sb.append( String.format( "%10.8f\n", plan[ i ] ) );
    }

    return sb.toString();
  }
}
