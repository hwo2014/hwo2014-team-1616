package bot;

import java.io.IOException;
import java.nio.file.Files;

import msg.MsgType;
import nn.F;
import nn.NnInstance;

@SuppressWarnings( { "nls", "boxing" } )
public final class DataCollector
{
  public static void collectData( final Tracker tr, final String... raceSets ) throws IOException
  {
    for( final String raceSet : raceSets )
    {
      U.log( "Loading %s...", raceSet );

      Files.list( U.RACE_REPO.resolve( raceSet ) ).forEach( raceFile ->
      {
        try
        {
          Files.lines( raceFile ).forEach( line ->
          {
            final int pos = line.indexOf( ' ' );
            //final int msgGameTick = Integer.parseInt( line.substring( 0, pos ) );
            final String msg = line.substring( pos + 1 );
            final MsgType t = MsgType.get( msg );
            if( t == null ) U.fail( "Unknown message type: %s", msg );
            tr.track( t, msg );
          } );
        }
        catch( final Exception e )
        {
          U.fail( e );
        }
      } );
    }

    U.log( "Loading completed, %d rows", tr.data.size() );
  }

  public static void trainPredictor
  ( final long seed, final String name, final F norm, final F activation,
    final int[] dataMapper, final int[] targetMapper )
  throws IOException
  {
    U.rng.setSeed( seed );

    final Tracker tr = new Tracker( dataMapper, targetMapper );
    collectData( tr, "set0" );

    final double[][] data   = tr.data.toArray( new double[][]{} );
    final double[][] target = tr.target.toArray( new double[][]{} );

//    try( PrintWriter pw = new PrintWriter( Files.newOutputStream( U.RACE_LOGS.resolve( "_" + name + "Data.txt" ) ) ) )
//    {
//      U.print( pw, data );
//    }
//
//    try( PrintWriter pw = new PrintWriter( Files.newOutputStream( U.RACE_LOGS.resolve( "_" + name + "Target.txt" ) ) ) )
//    {
//      U.print( pw, target );
//    }

    final NnInstance nn = new NnInstance
    ( name, norm, activation,
      dataMapper.length,
      10, Math.min( 6, dataMapper.length ),
      6, 6,
      targetMapper.length
    );

    nn.prepare( data, target );
    nn.learn( data, target, 100000 );
  }

  public static void main( final String... args ) throws IOException
  {
    U.init();

    final F norm = F.SQUARE;
    final F act = F.SIGMOID_TANH;

    trainPredictor( 4193638967711873286L, "vp", norm, act, DataBuffer.DATA, DataBuffer.TARGET );
  }
}
