package bot;

import static java.lang.Math.PI;
import static java.lang.Math.abs;
import static java.lang.Math.signum;
import static java.lang.Math.sqrt;

import java.io.PrintWriter;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Date;
import java.util.Locale;
import java.util.Random;
import java.util.Scanner;

import msg.Piece;
import msg.Track;

import org.ejml.data.DenseMatrix64F;

import com.google.gson.Gson;

@SuppressWarnings( { "nls", "boxing" } )
public class U
{
  public static final boolean REMOTE_RUN =
     "C:\\Program Files\\Java\\jdk8".equals( System.getenv( "JAVA_HOME" ) )
  || "/cygdrive/c/Program Files/Java/jdk8".equals( System.getenv( "JAVA_HOME" ) ) ;

  public static final Path    NN_REPO    = Paths.get( "\\prj\\data\\nn\\" );
  public static final Path    RACE_REPO  = Paths.get( "\\prj\\data\\races\\" );
  public static final Path    RACE_LOGS  = Paths.get( "\\prj\\data\\logs\\" );

  public static final String TIMESTAMP_FORMAT = "%tY%<tm%<td-%<tH%<tM%<tS";
  public static final String DOUBLE_F_FORMAT = "%22.18f";
  public static final String DOUBLE_G_FORMAT = "%24.18g";

  public static final Random  rng              = new Random();
  public static final Gson    gson             = new Gson();

  public static boolean       replay;

  public static String timestamp()
  {
    final Date now = new Date( System.currentTimeMillis() );
    return String.format( U.TIMESTAMP_FORMAT, now );
  }

  public static void init()
  {
    Locale.setDefault( Locale.US );
  }

  public static void fail( final Throwable t )
  {
    t.printStackTrace();
    fail( t.getMessage() );
  }

  public static void fail( final String msg, final Object... args )
  {
    throw new RuntimeException( String.format( msg, args ) );
  }

  public static void log( final String msg, final Object... args )
  {
    System.out.println( String.format( msg, args ) );
  }

  public static void print( final PrintWriter pw, final String name, final DenseMatrix64F m )
  {
    pw.printf( "# matrix %s[%dx%d]\n", name, m.getNumRows(), m.getNumCols() );

    for( int i = 0; i < m.getNumRows(); i++ )
    {
      for( int j = 0; j < m.getNumCols(); j++ )
      {
        pw.printf( " " + DOUBLE_G_FORMAT, m.get( i, j ) );
      }
      pw.println();
    }
  }

  public static void print( final PrintWriter pw, final double[][] m )
  {
    for( int i = 0; i < m.length; i++ )
    {
      for( final double x : m[ i ] )
      {
        pw.printf( " " + DOUBLE_G_FORMAT, x );
      }

      pw.println();
    }
  }

  public static void restore( final Scanner sc, final DenseMatrix64F m )
  {
    for( int i = 0; i < m.getNumRows(); i++ )
    {
      for( int j = 0; j < m.getNumCols(); j++ )
      {
        m.set( i, j, sc.nextDouble() );
      }
    }
  }

  public static Path getNnPath( final String nnName ) throws URISyntaxException
  {
    if( REMOTE_RUN )
      return U.NN_REPO.resolve( nnName );

    final URL in = U.class.getClassLoader().getResource( nnName );
    return Paths.get( in.toURI() );
  }

  public static String toString( final double[] a, final String format )
  {
    if( a == null )
      return "null";
    final int iMax = a.length - 1;
    if( iMax == -1 )
      return "[]";

    final StringBuilder b = new StringBuilder();
    b.append( '[' );
    for( int i = 0;; i++ )
    {
      b.append( String.format( format, a[ i ] ) );
      if( i == iMax )
        return b.append( ']' ).toString();
      b.append( ", " );
    }
  }

  public static <T> String toString( final T[] a )
  {
    if( a == null )
      return "null";

    final int iMax = a.length - 1;
    if( iMax == -1 )
      return "[]";

    final StringBuilder b = new StringBuilder();
    b.append( '[' );
    for( int i = 0;; i++ )
    {
      b.append( String.valueOf( a[ i ] ) );
      if( i == iMax )
        return b.append( ']' ).toString();
      b.append( "\n" );
    }
  }

  public static double[] map( final int[] mapper, final double[] data )
  {
    final double[] result = new double[ mapper.length ];

    for( int i = 0; i < mapper.length; i++ )
      result[ i ] = data[ mapper[ i ] ];

    return result;
  }

  public static void map( final int[] mapper, final double[] data, final double[] result )
  {
    for( int i = 0; i < mapper.length; i++ )
      result[ i ] = data[ mapper[ i ] ];
  }

  public static void rank1Update
  ( final double gamma, final DenseMatrix64F A, final DenseMatrix64F u, final DenseMatrix64F w )
  {
    final int m = u.getNumElements();
    final int n = w.getNumElements();

    int matrixIndex = 0;

    for( int i = 0; i < m; i++ )
    {
      final double elementU = u.data[ i ];

      for( int j = 0; j < n; j++ )
      {
        A.data[ matrixIndex++ ] += gamma * elementU * w.data[ j ];
      }
    }
  }

  public static double divideSafe( final double x, final double y )
  {
    return x == 0 && y == 0 ? 0 : x / y;
  }

  public static void normalize( final double[][] data, final double[] means, final double[] devs )
  {
    final int m = means.length;
    final int n = data.length;

    if( n == 0 || data[ 0 ].length != m || devs.length != m )
      U.fail( "Wrong sizes." );

    Arrays.fill( means, 0 );
    Arrays.fill( devs, 0 );

    for( int i = 0; i < m; i++ )
    {
      double sum = 0, sumSq = 0;

      for( final double[] row : data )
      {
        final double x = row[ i ];
        sum += x;
        sumSq += x*x;
      }

      means[ i ] = sum / n;
      devs[ i ] = sqrt( sumSq / n - means[ i ] * means[ i ] );
    }
  }

  public static void normalize( final double[] x, final double[] means, final double[] devs, final double[] y )
  {
    final int n = x.length;

    if( n != means.length || n != devs.length || n != y.length )
      U.fail( "Wrong sizes." );

    for( int i = 0; i < n; i++ )
      y[ i ] = divideSafe( ( x[ i ] - means[ i ] ), devs[ i ] );
  }

  public static void denormalize( final double[] x, final double[] means, final double[] devs, final double[] y )
  {
    final int n = x.length;

    if( n != means.length || n != devs.length || n != y.length )
      U.fail( "Wrong sizes." );

    for( int i = 0; i < n; i++ )
      y[ i ] = x[ i ] * devs[ i ] + means[ i ];
  }

  public static void setRandomNormal( final DenseMatrix64F m, final double mean, final double dev )
  {
    setRandomNormal( m.getData(), mean, dev );
  }

  public static void setRandomNormal( final double[] x, final double mean, final double dev )
  {
    for( int i = 0; i < x.length; i++ )
      x[ i ] = mean + dev * rng.nextGaussian();
  }

  public static double getLaneCurvature( final Track track, final Piece p, final int lane )
  {
    if( p.getLength() > 0 ) return 0;
    final double sign = signum( p.getAngle() );
    return sign / ( p.getRadius() - sign * track.getLanes()[ lane ].getDistanceFromCenter() );
  }

  public static double getLaneLength( final Track track, final Piece p, final int lane )
  {
    if( p.getLength() > 0 ) return p.getLength();

    return ( p.getRadius() - signum( p.getAngle() ) * track.getLanes()[ lane ].getDistanceFromCenter() )
         * abs( p.getAngle() ) * PI / 180;
  }
}
