package nn;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.Scanner;

import org.ejml.data.DenseMatrix64F;

import bot.U;

@SuppressWarnings( { "nls", "boxing" } )
public final class NnInstance
{
  public final String          name;
  public final F               lossNorm;
  public final F               activation;

  public final int[]           sizes;
  public final int             k;
  public final NnLayer[]       L;
  public final NnLayer         Lfirst;
  public final NnLayer         Llast;
  public final NnLayer[]       Lbackw;

  public final int             inputSize;
  public final int             outputSize;

  public final DenseMatrix64F  input;
  public final DenseMatrix64F  output;
  public final DenseMatrix64F  backPropInput;
  public final DenseMatrix64F  backPropOutput;

  public final DenseMatrix64F  inputMeans;
  public final DenseMatrix64F  inputDevs;
  public final DenseMatrix64F  outputMeans;
  public final DenseMatrix64F  outputDevs;

  public final Rprop          rprop;

  public NnInstance
  ( final String name1, final F lossNorm1, final F activation1, final int...sizes1 )
  {
    name = name1;
    sizes = Arrays.copyOf( sizes1, sizes1.length );
    k = sizes.length / 2;
    lossNorm = lossNorm1;
    activation = activation1;

    inputSize = sizes[ 0 ];
    outputSize = sizes[ 2*k - 1 ];

    input = new DenseMatrix64F( inputSize, 1 );

    L = new NnLayer[ k ];
    Lbackw = new NnLayer[ k ];

    Lfirst = L[ 0 ] = new NnLayer( sizes[ 1 ], sizes[ 2 ], input, activation );
    DenseMatrix64F layerInput = Lfirst.f;

    for( int i = 1; i < k - 1; i++ )
    {
      L[ i ] = new NnLayer( sizes[ 2 * i + 1 ], sizes[ 2 * i + 2 ], layerInput, activation );
      layerInput = L[ i ].f;
    }

    Llast = L[ k - 1 ] = new NnLayer( sizes[ 2*k - 1 ], 0, layerInput, activation );

    output = Llast.f;
    backPropInput = Llast.dLdf_temp;
    backPropOutput = new DenseMatrix64F( inputSize, 1 );

    for( int i = 1; i < k; i++ )
      L[ i ].gradientOutput = L[ i - 1 ].dLdf_temp;

    Lfirst.gradientOutput = backPropOutput;

    for( int i = 0; i < k; i++ )
    {
      Lbackw[ i ] = L[ k - 1 - i ];
    }

    rprop = new Rprop();

    for( final NnLayer nl : L )
      nl.prepareRprop( rprop );

    inputMeans  = new DenseMatrix64F( input .getNumRows(), 1 );
    inputDevs   = new DenseMatrix64F( input .getNumRows(), 1 );
    outputMeans = new DenseMatrix64F( output.getNumRows(), 1 );
    outputDevs  = new DenseMatrix64F( output.getNumRows(), 1 );
  }

  public void learn( final double[][] dataDenorm, final double[][] observationsDenorm, final int epochMax )
  {
    final int n = dataDenorm.length;

    if( n == 0 || n != observationsDenorm.length )
      U.fail( "Wrong sizes." );

    final String ts = U.timestamp();
    save( String.format( "nn-%s-%s-%06d.txt", name, ts, 0 ) );

    final double[][] data = new double[ n ][];
    final double[][] observations = new double[ n ][];

    for( int i = 0; i < n; i++ )
    {
      data[ i ] = new double[ input.getNumRows() ];
      observations[ i ] = new double[ output.getNumRows() ];
      U.normalize( dataDenorm[ i ], inputMeans.getData(), inputDevs.getData(), data[ i ] );
      U.normalize( observationsDenorm[ i ], outputMeans.getData(), outputDevs.getData(), observations[ i ] );
    }

    final double[] losses = new double[ outputSize ];
    final double[] lossesDenormalized = new double[ outputSize ];

    double loss = lossAndGradient( data, observations, 1 );
    rprop.init();

    for( int epoch = 1; epoch <= epochMax && !rprop.isConverged(); epoch++ )
    {
      rprop.update();
      loss = lossAndGradient( data, observations, Math.max( 0, 1. - epoch / 200. ) );
      epoch++;

      if( epoch % 10 == 0 )
      {
        lossDenormalizedProVar( data, observations, losses, lossesDenormalized );

        U.log
        ( "nn-%s:%05d   L: %12.8f   Ls:%s   LDs:%s",
          name, epoch,
          lossNorm.applyInverse( loss / n ),
          U.toString( losses, "%.4f" ),
          U.toString( lossesDenormalized, "%.4f" )
        );

        save( String.format( "nn-%s-%s-%06d-%014.10f.txt", name, ts, epoch, lossNorm.applyInverse( loss / n ) ) );
      }
    }
  }

  private double lossAndGradient( final double[][] data, final double[][] observations, final double noise )
  {
    rprop.resetGradients();

    double loss = 0;

    for( int i = 0; i < data.length; i++ )
    {
      final double[] row = data[ i ];

      if( noise == 0 )
      {
        for( int j = 0; j < inputSize; j++ )
          input.set( j, row[ j ] );
      }
      else
      {
        for( int j = 0; j < inputSize; j++ )
          input.set( j, row[ j ] + U.rng.nextGaussian() * noise );
      }

      feedForw();

      for( int j = 0; j < outputSize; j++ )
      {
        final double[] observation = observations[ i ];
        final double d = observation[ j ] - output.get( j );
        loss += lossNorm.apply( d );
        backPropInput.set( j, -lossNorm.applyDerivative( d ) );
      }

      backProp();
    }

    return loss;
  }

  public double loss( final double[][] data, final double[][] observations )
  {
    double loss = 0;

    for( int i = 0; i < data.length; i++ )
    {
      final double[] observation = observations[ i ];
      U.normalize( data[ i ], inputMeans.getData(), inputDevs.getData(), input.getData() );
      feedForw();
      for( int j = 0; j < outputSize; j++ )
        loss += lossNorm.apply( ( observation[ j ] - outputMeans.get( j ) ) / outputDevs.get( j ) - output.get( j ) );
    }

    return loss;
  }

  private void lossDenormalizedProVar
  ( final double[][] dataNormalized, final double[][] observationsNormalized,
    final double[] loss, final double[] lossDenormalized )
  {
    for( int i = 0; i < dataNormalized.length; i++ )
    {
      final double[] observation = observationsNormalized[ i ];
      input.setData( dataNormalized[ i ] );
      feedForw();
      for( int j = 0; j < outputSize; j++ )
        loss[ j ] += lossNorm.apply( observation[ j ] - output.get( j ) );
    }

    for( int j = 0; j < outputSize; j++ )
    {
      loss[ j ] = lossNorm.applyInverse( loss[ j ] / dataNormalized.length );
      lossDenormalized[ j ] = loss[ j ] * outputDevs.get( j );
    }
  }

  public void prepare( final double[][] data, final double[][] observations )
  {
    U.normalize( data, inputMeans.getData(), inputDevs.getData() );
    U.normalize( observations, outputMeans.getData(), outputDevs.getData() );

    for( final NnLayer nl : L )
      nl.prepare();
  }

  private void feedForw()
  {
    for( final NnLayer nl : L )
      nl.feedForw();
  }

  private void backProp()
  {
    for( final NnLayer nl : Lbackw )
      nl.backProp();
  }

  public void backPropSingle( final double[] backPropIn )
  {
    Llast.dLdf_temp.set( backPropIn.length, 1, true, backPropIn );

    for( final NnLayer nl : Lbackw )
      nl.backPropSingle();
  }

  public void predict( final double[] in, final double[] prediction )
  {
    U.normalize( in, inputMeans.getData(), inputDevs.getData(), input.getData() );
    feedForw();
    U.denormalize( output.getData(), outputMeans.getData(), outputDevs.getData(), prediction );
  }

  @Override
  public String toString()
  {
    final StringWriter sw = new StringWriter();
    print( new PrintWriter( sw ) );
    return sw.toString();
  }

  public void print( final PrintWriter pw )
  {
    pw.printf( "# name, loss norm, activation\n%s %s %s\n", name, lossNorm, activation );

    pw.printf( "# sizes\n%d  ", sizes.length );
    for( final int s : sizes ) pw.printf( " %d", s );
    pw.println();

    U.print( pw, "inputMeans",  inputMeans );
    U.print( pw, "inputDevs",   inputDevs );
    U.print( pw, "outputMeans", outputMeans );
    U.print( pw, "outputDevs",  outputDevs );

    for( int i = 0; i < k; i++ )
    {
      final NnLayer nl = L[ i ];
      pw.printf( "# layer %d s=%d n=%d activation=%s\n", i+1, nl.s, nl.n, nl.activation );
      nl.print( pw );
    }
  }

  public void save( final String fileName )
  {
    try( BufferedWriter bw = Files.newBufferedWriter( U.RACE_LOGS.resolve( fileName ) ) )
    {
      bw.write( toString() );
    }
    catch( final IOException e )
    {
      U.fail( e );
    }
  }

  public void restore( final Scanner sc )
  {
    U.restore( sc, inputMeans );
    U.restore( sc, inputDevs );
    U.restore( sc, outputMeans );
    U.restore( sc, outputDevs );

    for( final NnLayer nl : L )
      nl.restore( sc );
  }

  public static NnInstance load( final Path path )
  {
    U.init();

    final StringBuilder sb = new StringBuilder();

    try( final BufferedReader br = Files.newBufferedReader( path ) )
    {
      String line;

      while( ( line = br.readLine() ) != null )
      {
        if( !line.startsWith( "#" ) )
        {
          sb.append( line );
          sb.append( "\n" );
        }
      }
    }
    catch( final IOException e )
    {
      e.printStackTrace();
    }

    @SuppressWarnings( "resource" )
    final Scanner sc = new Scanner( sb.toString() );

    final String name = sc.next();
    final F lossNorm = F.valueOf( sc.next() );
    final F activation = F.valueOf( sc.next() );
    final int n = sc.nextInt();
    final int[] sz = new int[ n ];

    for( int i = 0; i < n; i++ )
      sz[ i ] = sc.nextInt();

    final NnInstance nn = new NnInstance( name, lossNorm, activation, sz );
    nn.restore( sc );
    sc.close();
    return nn;
  }
}
