package nn;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Scanner;

import org.ejml.alg.dense.mult.VectorVectorMult;
import org.ejml.data.DenseMatrix64F;
import org.ejml.ops.CommonOps;

import bot.U;

@SuppressWarnings( { "nls", "boxing" } )
public final class NnLayer
{
  public final int            s;
  public final int            n;
  public final DenseMatrix64F input;
  public DenseMatrix64F       gradientOutput;

  public final F              activation;

  public final DenseMatrix64F f;
  public final DenseMatrix64F W;
  public final DenseMatrix64F b;

  public final DenseMatrix64F activations;

  public final DenseMatrix64F f_delta;
  public final DenseMatrix64F W_delta;
  public final DenseMatrix64F b_delta;

  public final DenseMatrix64F dLdf_temp;

  public final DenseMatrix64F dLdf;
  public final DenseMatrix64F dLdW;
  public final DenseMatrix64F dLdb;

  public final DenseMatrix64F dLdf0;
  public final DenseMatrix64F dLdW0;
  public final DenseMatrix64F dLdb0;

  public final NnNode[]       nodes;

  public NnLayer( final int s1, final int n1, final DenseMatrix64F input1, final F activation1 )
  {
    s = s1;
    n = n1;
    input = input1;
    activation = activation1;

    final int s_prev = input.getNumRows();

    f = new DenseMatrix64F( s, 1 );
    W = new DenseMatrix64F( s, s_prev );
    b = new DenseMatrix64F( s, 1 );

    activations = new DenseMatrix64F( s, 1 );

    f_delta = new DenseMatrix64F( s, 1 );
    W_delta = new DenseMatrix64F( s, s_prev );
    b_delta = new DenseMatrix64F( s, 1 );

    dLdf_temp = new DenseMatrix64F( s, 1 );

    dLdf = new DenseMatrix64F( s, 1 );
    dLdW = new DenseMatrix64F( s, s_prev );
    dLdb = new DenseMatrix64F( s, 1 );

    dLdf0 = new DenseMatrix64F( s, 1 );
    dLdW0 = new DenseMatrix64F( s, s_prev );
    dLdb0 = new DenseMatrix64F( s, 1 );

    if( n > 0 )
    {
      nodes = new NnNode[ s ];

      for( int j = 0; j < s; j++ )
        nodes[ j ] = new NnNode( n, input );
    }
    else
    {
      nodes = null;
    }
  }

  public void prepare()
  {
    U.setRandomNormal( W, 0, 1e-4 );
    U.setRandomNormal( b, 0, 1e-4 );

    if( n > 0 )
    {
      for( final NnNode node : nodes )
        node.prepare();
    }
  }

  public void feedForw()
  {
    f.set( b );
    CommonOps.multAdd( W, input, f );

    if( n > 0 )
    {
      for( int j = 0; j < s; j++ )
        activations.set( j, activation.apply( nodes[ j ].feedForw() ) );

      CommonOps.addEquals( f, activations );
    }
  }

  public void backProp()
  {
    CommonOps.addEquals( dLdf, dLdf_temp );
    VectorVectorMult.addOuterProd( 1, dLdf_temp, input, dLdW );
    CommonOps.addEquals( dLdb, dLdf_temp );

    if( gradientOutput != null )
      CommonOps.multTransA( W, dLdf_temp, gradientOutput );

    if( n > 0 )
    {
      for( int j = 0; j < s; j++ )
      {
        final NnNode node = nodes[ j ];
        node.q = dLdf_temp.get( j ) * activation.applyDerivative( node.c );
        node.backProp();
      }

      if( gradientOutput != null )
      {
        for( final NnNode node : nodes )
          CommonOps.subEquals( gradientOutput, node.dLdd_temp );
      }
    }
  }

  public void backPropSingle()
  {
    CommonOps.multTransA( W, dLdf_temp, gradientOutput );

    if( n > 0 )
    {
      for( int j = 0; j < s; j++ )
      {
        final NnNode node = nodes[ j ];
        node.q = dLdf_temp.get( j ) * activation.applyDerivative( node.c );
        node.backPropSingle();
        CommonOps.subEquals( gradientOutput, node.dLdd_temp );
      }
    }
  }

  public void prepareRprop( final Rprop rprop )
  {
    rprop.add( f.getData(), f_delta.getData(), dLdf0.getData(), dLdf.getData() );
    rprop.add( W.getData(), W_delta.getData(), dLdW0.getData(), dLdW.getData() );
    rprop.add( b.getData(), b_delta.getData(), dLdb0.getData(), dLdb.getData() );

    if( n > 0 )
    {
      for( final NnNode node : nodes )
        node.prepareRprop( rprop );
    }
  }

  @Override
  public String toString()
  {
    final StringWriter sw = new StringWriter();
    print( new PrintWriter( sw ) );
    return sw.toString();
  }

  public void print( final PrintWriter pw )
  {
    U.print( pw, "W", W );
    U.print( pw, "b", b );

    if( n > 0 )
    {
      for( int i = 0; i < s; i++ )
      {
        pw.printf( "# node %d\n", i+1 );
        nodes[ i ].print( pw );
      }
    }
  }

  public void restore( final Scanner sc )
  {
    U.restore( sc, W );
    U.restore( sc, b );

    if( n > 0 )
    {
      for( final NnNode node : nodes )
      {
        node.restore( sc );
      }
    }
  }
}
