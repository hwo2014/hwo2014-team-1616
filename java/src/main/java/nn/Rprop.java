package nn;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public final class Rprop
{
  public static final double DELTA0          = 0.1;
  public static final double MIN_DELTA       = 1e-10;
  public static final double MAX_DELTA       = 100;

  public static final double FORWARD_FACTOR  = 1.2;
  public static final double BACKWARD_FACTOR = 0.5;

  public List<RpropGroup>    groups = new ArrayList<>();
  private double             lastMaxDelta;
  private boolean            converged;

  public void add( final double[] v, final double[] d, final double[] g0, final double[] g )
  {
    groups.add( new RpropGroup( v, d, g0, g ) );
  }

  public void init()
  {
    converged = false;

    for( final RpropGroup grp : groups )
    {
      Arrays.fill( grp.deltas, DELTA0 );
      Arrays.fill( grp.gradientsBefore, 0 );
    }
  }

  public void update()
  {
    converged = false;
    double maxDelta = 0;

    for( final RpropGroup grp : groups )
    {
      final double delta = grp.update();
      if( maxDelta < delta ) maxDelta = delta;
    }

    converged = ( lastMaxDelta = maxDelta ) <= MIN_DELTA;
  }

  public void resetGradients()
  {
    for( final RpropGroup grp : groups )
    {
      Arrays.fill( grp.gradients, 0 );
    }
  }

  public boolean isConverged()
  {
    return converged;
  }

  public double getLastMaxDelta()
  {
    return lastMaxDelta;
  }
}
