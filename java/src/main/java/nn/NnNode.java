package nn;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Scanner;

import org.ejml.alg.dense.mult.VectorVectorMult;
import org.ejml.data.DenseMatrix64F;
import org.ejml.ops.CommonOps;

import bot.U;

@SuppressWarnings( { "nls" } )
public final class NnNode
{
  public final int            n;

  public final DenseMatrix64F d;
  public final DenseMatrix64F P;
  public final DenseMatrix64F g;

  public final DenseMatrix64F f;

  public final DenseMatrix64F d_delta;
  public final DenseMatrix64F P_delta;
  public final DenseMatrix64F g_delta;

  public final DenseMatrix64F dLdd_temp;

  public final DenseMatrix64F dLdd;
  public final DenseMatrix64F dLdP;
  public final DenseMatrix64F dLdg;

  public final DenseMatrix64F dLdd0;
  public final DenseMatrix64F dLdP0;
  public final DenseMatrix64F dLdg0;

  public final DenseMatrix64F z;
  public final DenseMatrix64F Pz;
  public final DenseMatrix64F GPz;

  public double               c;
  public double               q;

  public NnNode( final int n1, final DenseMatrix64F input )
  {
    final int s_prev = input.getNumRows();
    n = n1;

    d = new DenseMatrix64F( s_prev, 1 );
    P = new DenseMatrix64F( n, s_prev );
    g = new DenseMatrix64F( n, 1 );

    f = input;

    d_delta = new DenseMatrix64F( s_prev, 1 );
    P_delta = new DenseMatrix64F( n, s_prev );
    g_delta = new DenseMatrix64F( n, 1 );

    dLdd_temp = new DenseMatrix64F( s_prev, 1 );

    dLdd = new DenseMatrix64F( s_prev, 1 );
    dLdP = new DenseMatrix64F( n, s_prev );
    dLdg = new DenseMatrix64F( n, 1 );

    dLdd0 = new DenseMatrix64F( s_prev, 1 );
    dLdP0 = new DenseMatrix64F( n, s_prev );
    dLdg0 = new DenseMatrix64F( n, 1 );

    z = new DenseMatrix64F( s_prev, 1 );
    Pz = new DenseMatrix64F( n, 1 );
    GPz = new DenseMatrix64F( n, 1 );
  }

  public void prepare()
  {
    U.setRandomNormal( d, 0, 1 );
    U.setRandomNormal( P, 0, 1e-1 );
    U.setRandomNormal( g, 0, 1e-1 );
  }

  public double feedForw()
  {
    CommonOps.sub( f, d, z );
    CommonOps.mult( P, z, Pz );
    CommonOps.elementMult( g, Pz, GPz );
    c = VectorVectorMult.innerProd( Pz, GPz );
    return c;
  }

  public void backProp()
  {
    CommonOps.multTransA( -2*q, P, GPz, dLdd_temp );
    CommonOps.addEquals( dLdd, dLdd_temp );

    U.rank1Update( 2*q, dLdP, GPz, z );

    for( int i = 0; i < n; i++ )
    {
      final double x = Pz.get( i );
      dLdg.getData()[ i ] += q*x*x;
    }
  }

  public void backPropSingle()
  {
    CommonOps.multTransA( -2*q, P, GPz, dLdd_temp );
  }

  public void prepareRprop( final Rprop rprop )
  {
    rprop.add( d.getData(), d_delta.getData(), dLdd0.getData(), dLdd.getData() );
    rprop.add( P.getData(), P_delta.getData(), dLdP0.getData(), dLdP.getData() );
    rprop.add( g.getData(), g_delta.getData(), dLdg0.getData(), dLdg.getData() );
  }

  @Override
  public String toString()
  {
    final StringWriter sw = new StringWriter();
    print( new PrintWriter( sw ) );
    return sw.toString();
  }

  public void print( final PrintWriter pw )
  {
    U.print( pw, "d", d );
    U.print( pw, "P", P );
    U.print( pw, "g", g );
  }

  public void restore( final Scanner sc )
  {
    U.restore( sc, d );
    U.restore( sc, P );
    U.restore( sc, g );
  }
}
