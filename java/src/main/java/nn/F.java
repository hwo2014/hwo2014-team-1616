package nn;

import static java.lang.Math.abs;
import static java.lang.Math.log;
import static java.lang.Math.signum;
import static java.lang.Math.sqrt;
import static java.lang.Math.tanh;
import bot.U;

@SuppressWarnings( "nls" )
public enum F
{
  ID()
  {
    @Override
    public double apply( final double x )
    {
      return x;
    }

    @Override
    public double applyDerivative( final double x )
    {
      return 1;
    }

    @Override
    public double applyInverse( final double x )
    {
      return x;
    }
  },

  ABS()
  {
    @Override
    public double apply( final double x )
    {
      return abs( x );
    }

    @Override
    public double applyDerivative( final double x )
    {
      return signum( x );
    }

    @Override
    public double applyInverse( final double x )
    {
      if( x < 0 ) U.fail( "abs inverse not defined in " + x );
      return x;
    }
  },

  SQUARE()
  {
    @Override
    public double apply( final double x )
    {
      return x * x;
    }

    @Override
    public double applyDerivative( final double x )
    {
      return 2 * x;
    }

    @Override
    public double applyInverse( final double x )
    {
      if( x < 0 ) U.fail( "square root not defined in " + x );
      return sqrt( x );
    }
  },

  QUAD()
  {
    @Override
    public double apply( final double x )
    {
      return x * x * x * x;
    }

    @Override
    public double applyDerivative( final double x )
    {
      return 4 * x * x * x;
    }

    @Override
    public double applyInverse( final double x )
    {
      if( x < 0 ) U.fail( "quad root not defined in " + x );
      return sqrt( sqrt( x ) );
    }
  },

  BULB_SQUARE1()
  {
    @Override
    public double apply( final double x )
    {
      return 1 / ( 1 + x * x );
    }

    @Override
    public double applyDerivative( final double x )
    {
      final double y = 1 + x * x;
      return - 2 * x / ( y * y );
    }

    @Override
    public double applyInverse( final double x )
    {
      U.fail( "bulb-square1 inverse not defined" );
      return 0;
    }
  },

  SIGMOID_XdivSqrt1pX2()
  {
    @Override
    public double apply( final double x )
    {
      return x / sqrt( 1 + x*x );
    }

    @Override
    public double applyDerivative( final double x )
    {
      final double z = 1 + x*x;
      return 1 / ( z * Math.sqrt( z ) ) ;
    }

    @Override
    public double applyInverse( final double x )
    {
      if( x <= -1 || x >= 1 )
        U.fail( "inverse not defined in " + x );

      return x / sqrt( 1 - x*x );
    }
  },

  SIGMOID_Xdiv1pAbsX()
  {
    @Override
    public double apply( final double x )
    {
      return x / ( 1 + abs( x ) );
    }

    @Override
    public double applyDerivative( final double x )
    {
      final double z = 1 + abs( x );
      return 1 / ( z * z ) ;
    }

    @Override
    public double applyInverse( final double x )
    {
      if( x <= -1 || x >= 1 )
        U.fail( "inverse not defined in " + x );

      return x / ( 1 - abs( x ) );
    }
  },

  SIGMOID_TANH()
  {
    @Override
    public double apply( final double x )
    {
      return tanh( x );
    }

    @Override
    public double applyDerivative( final double x )
    {
      final double tanh = apply( x );
      return 1 - tanh * tanh;
    }

    @Override
    public double applyInverse( final double x )
    {
      if( x <= -1 || x >= 1 )
        U.fail( "atanh not defined in " + x );
      return log( ( 1 + x ) / ( 1 - x ) ) / 2;
    }
  };

  public abstract double apply( double x );
  public abstract double applyDerivative( double x );
  public abstract double applyInverse( double x );
}
