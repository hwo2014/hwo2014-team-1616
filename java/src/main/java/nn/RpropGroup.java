package nn;

import static java.lang.Math.max;
import static java.lang.Math.min;
import static java.lang.Math.signum;
import static nn.Rprop.BACKWARD_FACTOR;
import static nn.Rprop.FORWARD_FACTOR;
import static nn.Rprop.MAX_DELTA;
import static nn.Rprop.MIN_DELTA;
import bot.U;

@SuppressWarnings( "nls" )
public final class RpropGroup
{
  public final int      size;
  public final double[] values;
  public final double[] deltas;
  public final double[] gradientsBefore;
  public final double[] gradients;

  public RpropGroup( final double[] values1, final double[] deltas1, final double[] gradientsBefore1, final double[] gradients1 )
  {
    values = values1;
    deltas = deltas1;
    gradientsBefore = gradientsBefore1;
    gradients = gradients1;

    size = values.length;

    if(    size != deltas.length
        || size != gradientsBefore.length
        || size != gradients.length )
    {
      U.fail( "RpropGroup inconsistent array lengths" );
    }
  }

  public double update()
  {
    double maxDelta = 0;

    for( int i = 0; i < size; i++ )
    {
      final double gprod = gradientsBefore[ i ] * gradients[ i ];
      final double delta;

      if( gprod > 0 )
      {
        delta = deltas[ i ] = min( deltas[ i ] * FORWARD_FACTOR, MAX_DELTA );
        values[ i ] -= signum( gradients[ i ] ) * delta;
        gradientsBefore[ i ] = gradients[ i ];
      }
      else if( gprod < 0 )
      {
        delta = deltas[ i ] = max( deltas[ i ] * BACKWARD_FACTOR, MIN_DELTA );
        gradientsBefore[ i ] = 0;
      }
      else // gprod == 0
      {
        delta = deltas[ i ];
        values[ i ] -= signum( gradients[ i ] ) * delta;
        gradientsBefore[ i ] = gradients[ i ];
      }

      if( maxDelta < delta ) maxDelta = delta;
    }

    return maxDelta;
  }
}
