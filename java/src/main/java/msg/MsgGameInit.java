package msg;

public final class MsgGameInit
{
  private String       msgType;
  private GameInitData data;
  private String       gameId;

  public String getMsgType()
  {
    return msgType;
  }

  public void setMsgType( final String msgType1 )
  {
    msgType = msgType1;
  }

  public GameInitData getData()
  {
    return data;
  }

  public void setData( final GameInitData data1 )
  {
    data = data1;
  }

  public String getGameId()
  {
    return gameId;
  }

  public void setGameId( final String gameId1 )
  {
    gameId = gameId1;
  }
}
