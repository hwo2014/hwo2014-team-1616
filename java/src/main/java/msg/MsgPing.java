package msg;

public final class MsgPing
{
  private String msgType;

  public MsgPing()
  {
    msgType = MsgType.PING.type();
  }

  public String getMsgType()
  {
    return msgType;
  }

  public void setMsgType( final String msgType1 )
  {
    msgType = msgType1;
  }
}
