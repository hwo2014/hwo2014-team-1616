package msg;

public final class RaceSession
{
  private int     laps;
  private int     maxLapTimeMs;
  private boolean quickRace;

  private int     durationMs; // quali in full race only

  public int getLaps()
  {
    return laps;
  }

  public void setLaps( final int laps1 )
  {
    laps = laps1;
  }

  public int getMaxLapTimeMs()
  {
    return maxLapTimeMs;
  }

  public void setMaxLapTimeMs( final int maxLapTimeMs1 )
  {
    maxLapTimeMs = maxLapTimeMs1;
  }

  public boolean isQuickRace()
  {
    return quickRace;
  }

  public void setQuickRace( final boolean quickRace1 )
  {
    quickRace = quickRace1;
  }

  public int getDurationMs()
  {
    return durationMs;
  }

  public void setDurationMs( final int durationMs1 )
  {
    durationMs = durationMs1;
  }
}
