
package msg;

public final class Result
{
  private Car       car;
  private RaceTime result;

  public Car getCar()
  {
    return car;
  }

  public void setCar( final Car car1 )
  {
    car = car1;
  }

  public RaceTime getResult()
  {
    return result;
  }

  public void setResult( final RaceTime result1 )
  {
    result = result1;
  }
}
