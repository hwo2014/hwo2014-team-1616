package msg;

public final class Dimensions
{
  private double length;
  private double width;
  private double guideFlagPosition;

  public double getLength()
  {
    return length;
  }

  public void setLength( final double length1 )
  {
    length = length1;
  }

  public double getWidth()
  {
    return width;
  }

  public void setWidth( final double width1 )
  {
    width = width1;
  }

  public double getGuideFlagPosition()
  {
    return guideFlagPosition;
  }

  public void setGuideFlagPosition( final double guideFlagPosition1 )
  {
    guideFlagPosition = guideFlagPosition1;
  }
}
