package msg;

public final class MsgYourCar
{
  private String msgType;
  private Car    data;

  public String getMsgType()
  {
    return msgType;
  }

  public void setMsgType( final String msgType1 )
  {
    msgType = msgType1;
  }

  public Car getData()
  {
    return data;
  }

  public void setData( final Car data1 )
  {
    data = data1;
  }
}
