package msg;

public final class Car
{
  private String name;
  private String color;

  public String getName()
  {
    return name;
  }

  public void setName( final String name1 )
  {
    name = name1;
  }

  public String getColor()
  {
    return color;
  }

  public void setColor( final String color1 )
  {
    color = color1;
  }
}
