package msg;

public final class RaceData
{
  private BotData botId;
  private String  trackName;
  private String  password;
  private int     carCount;

  public BotData getBotId()
  {
    return botId;
  }

  public void setBotId( final BotData botId1 )
  {
    botId = botId1;
  }

  public String getTrackName()
  {
    return trackName;
  }

  public void setTrackName( final String trackName1 )
  {
    trackName = trackName1;
  }

  public String getPassword()
  {
    return password;
  }

  public void setPassword( final String password1 )
  {
    password = password1;
  }

  public int getCarCount()
  {
    return carCount;
  }

  public void setCarCount( final int carCount1 )
  {
    carCount = carCount1;
  }
}
