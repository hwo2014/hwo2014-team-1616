package msg;

public final class MsgCreateRace
{
  private String   msgType;
  private RaceData data;

  public MsgCreateRace
  ( final String botName, final String botKey,
    final String trackName, final String password, final int carCount )
  {
    msgType = MsgType.CREATE_RACE.type();
    data = new RaceData();
    data.setBotId( new BotData( botName, botKey ) );
    data.setTrackName( trackName );
    data.setPassword( password );
    data.setCarCount( carCount );
  }

  public String getMsgType()
  {
    return msgType;
  }

  public void setMsgType( final String msgType1 )
  {
    msgType = msgType1;
  }

  public RaceData getData()
  {
    return data;
  }

  public void setData( final RaceData data1 )
  {
    data = data1;
  }
}
