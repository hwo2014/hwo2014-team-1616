package msg;

public final class MsgSwitchLane
{
  private String msgType;
  private String data;

  public MsgSwitchLane( final String action )
  {
    msgType = MsgType.SWITCH_LANE.type();
    data = action;
  }

  public String getMsgType()
  {
    return msgType;
  }

  public void setMsgType( final String msgType1 )
  {
    msgType = msgType1;
  }

  public String getData()
  {
    return data;
  }

  public void setData( final String data1 )
  {
    data = data1;
  }
}
