package msg;

public class BotData
{
  private String name;
  private String key;

  public BotData( final String botName, final String botKey )
  {
    name = botName;
    key = botKey;
  }

  public String getName()
  {
    return name;
  }

  public void setName( final String name1 )
  {
    name = name1;
  }

  public String getKey()
  {
    return key;
  }

  public void setKey( final String key1 )
  {
    key = key1;
  }
}
