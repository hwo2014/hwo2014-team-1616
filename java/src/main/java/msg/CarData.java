package msg;

public final class CarData
{
  private Car        id;
  private Dimensions dimensions;

  public Car getId()
  {
    return id;
  }

  public void setId( final Car id1 )
  {
    id = id1;
  }

  public Dimensions getDimensions()
  {
    return dimensions;
  }

  public void setDimensions( final Dimensions dimensions1 )
  {
    dimensions = dimensions1;
  }

  public double getMassCenterRotationRadius()
  {
    return dimensions.getLength() / 2 - dimensions.getGuideFlagPosition();
  }
}
