package msg;

public final class Lane
{
  private int startLaneIndex;
  private int endLaneIndex;

  public int getStartLaneIndex()
  {
    return startLaneIndex;
  }

  public void setStartLaneIndex( final int startLaneIndex1 )
  {
    startLaneIndex = startLaneIndex1;
  }

  public int getEndLaneIndex()
  {
    return endLaneIndex;
  }

  public void setEndLaneIndex( final int endLaneIndex1 )
  {
    endLaneIndex = endLaneIndex1;
  }
}
