package msg;

public final class StartingPoint
{
  private Position position;
  private double   angle;

  public Position getPosition()
  {
    return position;
  }

  public void setPosition( final Position position1 )
  {
    position = position1;
  }

  public double getAngle()
  {
    return angle;
  }

  public void setAngle( final double angle1 )
  {
    angle = angle1;
  }
}
