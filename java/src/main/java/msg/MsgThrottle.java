package msg;

public final class MsgThrottle
{
  private String msgType;
  private double data;

  public MsgThrottle( final double throttle )
  {
    msgType = MsgType.THROTTLE.type();
    data = throttle;
  }

  public String getMsgType()
  {
    return msgType;
  }

  public void setMsgType( final String msgType1 )
  {
    msgType = msgType1;
  }

  public double getData()
  {
    return data;
  }

  public void setData( final double data1 )
  {
    data = data1;
  }
}
