package msg;

public final class Ranking
{
  private int overall;
  private int fastestLap;

  public int getOverall()
  {
    return overall;
  }

  public void setOverall( final int overall1 )
  {
    overall = overall1;
  }

  public int getFastestLap()
  {
    return fastestLap;
  }

  public void setFastestLap( final int fastestLap1 )
  {
    fastestLap = fastestLap1;
  }
}
