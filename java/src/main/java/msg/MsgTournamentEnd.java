package msg;

public final class MsgTournamentEnd
{
  private String msgType;
  private Object data;

  public String getMsgType()
  {
    return msgType;
  }

  public void setMsgType( final String msgType1 )
  {
    msgType = msgType1;
  }

  public Object getData()
  {
    return data;
  }

  public void setData( final Object data1 )
  {
    data = data1;
  }
}
