package msg;

public final class MsgTurboAvailable
{
  private String    msgType;
  private TurboData data;
  private String    gameId;
  private int       gameTick;

  public String getMsgType()
  {
    return msgType;
  }

  public void setMsgType( final String msgType1 )
  {
    msgType = msgType1;
  }

  public TurboData getData()
  {
    return data;
  }

  public void setData( final TurboData data1 )
  {
    data = data1;
  }

  public String getGameId()
  {
    return gameId;
  }

  public void setGameId( final String gameId1 )
  {
    gameId = gameId1;
  }

  public int getGameTick()
  {
    return gameTick;
  }

  public void setGameTick( final int gameTick1 )
  {
    gameTick = gameTick1;
  }
}
