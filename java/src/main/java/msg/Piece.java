package msg;

import com.google.gson.annotations.SerializedName;

@SuppressWarnings( "nls" )
public final class Piece
{
  private double  length;
  @SerializedName( "switch" )
  private boolean _switch;
  private int     radius;
  private double  angle;

  public double getLength()
  {
    return length;
  }

  public void setLength( final double length1 )
  {
    length = length1;
  }

  public boolean isSwitch()
  {
    return _switch;
  }

  public void setSwitch( final boolean _switch1 )
  {
    _switch = _switch1;
  }

  public int getRadius()
  {
    return radius;
  }

  public void setRadius( final int radius1 )
  {
    radius = radius1;
  }

  public double getAngle()
  {
    return angle;
  }

  public void setAngle( final double angle1 )
  {
    angle = angle1;
  }

  @Override
  public String toString()
  {
    return "Piece [length=" + length + ", _switch=" + _switch + ", radius=" + radius + ", angle=" + angle + "]";
  }
}
