package msg;

public final class FailureData
{
  private Car    car;
  private String reason;

  public Car getCar()
  {
    return car;
  }

  public void setCar( final Car car1 )
  {
    car = car1;
  }

  public String getReason()
  {
    return reason;
  }

  public void setReason( final String reason1 )
  {
    reason = reason1;
  }
}
