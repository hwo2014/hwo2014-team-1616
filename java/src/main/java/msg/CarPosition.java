package msg;

import static java.lang.Math.PI;

public final class CarPosition
{
  private Car           id;
  private double        angle;
  private PiecePosition piecePosition;

  public Car getId()
  {
    return id;
  }

  public void setId( final Car id1 )
  {
    id = id1;
  }

  public double getAngle()
  {
    return angle;
  }

  public void setAngle( final double angle1 )
  {
    angle = angle1;
  }

  public PiecePosition getPiecePosition()
  {
    return piecePosition;
  }

  public void setPiecePosition( final PiecePosition piecePosition1 )
  {
    piecePosition = piecePosition1;
  }

  public double getAngleInRadians()
  {
    return PI * angle / 180.0;
  }
}
