package msg;

public final class MsgCrash
{
  private String msgType;
  private Car    data;
  private String gameId;
  private int    gameTick;

  public String getMsgType()
  {
    return msgType;
  }

  public void setMsgType( final String msgType1 )
  {
    msgType = msgType1;
  }

  public Car getData()
  {
    return data;
  }

  public void setData( final Car data1 )
  {
    data = data1;
  }

  public String getGameId()
  {
    return gameId;
  }

  public void setGameId( final String gameId1 )
  {
    gameId = gameId1;
  }

  public int getGameTick()
  {
    return gameTick;
  }

  public void setGameTick( final int gameTick1 )
  {
    gameTick = gameTick1;
  }
}
