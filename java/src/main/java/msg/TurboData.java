package msg;

public final class TurboData
{
  private double turboDurationMilliseconds;
  private int    turboDurationTicks;
  private double turboFactor;

  public double getTurboDurationMilliseconds()
  {
    return turboDurationMilliseconds;
  }

  public void setTurboDurationMilliseconds( final double turboDurationMilliseconds1 )
  {
    turboDurationMilliseconds = turboDurationMilliseconds1;
  }

  public int getTurboDurationTicks()
  {
    return turboDurationTicks;
  }

  public void setTurboDurationTicks( final int turboDurationTicks1 )
  {
    turboDurationTicks = turboDurationTicks1;
  }

  public double getTurboFactor()
  {
    return turboFactor;
  }

  public void setTurboFactor( final double turboFactor1 )
  {
    turboFactor = turboFactor1;
  }
}
