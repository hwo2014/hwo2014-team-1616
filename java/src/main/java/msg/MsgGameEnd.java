package msg;

public final class MsgGameEnd
{
  private String      msgType;
  private GameEndData data;

  public String getMsgType()
  {
    return msgType;
  }

  public void setMsgType( final String msgType1 )
  {
    msgType = msgType1;
  }

  public GameEndData getData()
  {
    return data;
  }

  public void setData( final GameEndData data1 )
  {
    data = data1;
  }
}
