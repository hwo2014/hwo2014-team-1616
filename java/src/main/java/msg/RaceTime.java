package msg;

public final class RaceTime
{
  private int laps;
  private int ticks;
  private int millis;

  public int getLaps()
  {
    return laps;
  }

  public void setLaps( final int laps1 )
  {
    laps = laps1;
  }

  public int getTicks()
  {
    return ticks;
  }

  public void setTicks( final int ticks1 )
  {
    ticks = ticks1;
  }

  public int getMillis()
  {
    return millis;
  }

  public void setMillis( final int millis1 )
  {
    millis = millis1;
  }
}
