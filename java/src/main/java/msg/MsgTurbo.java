package msg;

public final class MsgTurbo
{
  private String    msgType;
  private String    data;

  public MsgTurbo( final String message )
  {
    msgType = MsgType.TURBO.type();
    data = message;
  }

  public String getMsgType()
  {
    return msgType;
  }

  public void setMsgType( final String msgType1 )
  {
    msgType = msgType1;
  }

  public String getData()
  {
    return data;
  }

  public void setData( final String data1 )
  {
    data = data1;
  }
}
