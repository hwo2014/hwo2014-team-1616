package msg;

public final class MsgJoin
{
  private String  msgType;
  private BotData data;

  public String getMsgType()
  {
    return msgType;
  }

  public MsgJoin( final String botName, final String botKey )
  {
    msgType = MsgType.JOIN.type();
    data = new BotData( botName, botKey );
  }

  public BotData getData()
  {
    return data;
  }

  public void setData( final BotData data1 )
  {
    data = data1;
  }
}
