package msg;

public final class MsgJoinRace
{
  private String   msgType;
  private RaceData data;

  public MsgJoinRace
  ( final String botName, final String botKey,
    final String trackName, final String password, final int carCount )
    {
      msgType = MsgType.JOIN_RACE.type();
      data = new RaceData();
      data.setBotId( new BotData( botName, botKey ) );
      data.setTrackName( trackName );
      data.setPassword( password );
      data.setCarCount( carCount );
    }

  public String getMsgType()
  {
    return msgType;
  }

  public void setMsgType( final String msgType1 )
  {
    msgType = msgType1;
  }

  public RaceData getData()
  {
    return data;
  }

  public void setData( final RaceData data1 )
  {
    data = data1;
  }
}
