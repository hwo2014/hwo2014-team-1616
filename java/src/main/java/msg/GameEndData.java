package msg;

public final class GameEndData
{
  private Result[] results;
  private Result[] bestLaps;

  public Result[] getResults()
  {
    return results;
  }

  public void setResults( final Result[] results1 )
  {
    results = results1;
  }

  public Result[] getBestLaps()
  {
    return bestLaps;
  }

  public void setBestLaps( final Result[] bestLaps1 )
  {
    bestLaps = bestLaps1;
  }
}
