package msg;

public final class GameInitData
{
  private Race race;

  public Race getRace()
  {
    return race;
  }

  public void setRace( final Race race1 )
  {
    race = race1;
  }
}
