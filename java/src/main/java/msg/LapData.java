package msg;

public final class LapData
{
  private Car      car;
  private LapTime  lapTime;
  private RaceTime raceTime;
  private Ranking  ranking;

  public Car getCar()
  {
    return car;
  }

  public void setCar( final Car car1 )
  {
    car = car1;
  }

  public LapTime getLapTime()
  {
    return lapTime;
  }

  public void setLapTime( final LapTime lapTime1 )
  {
    lapTime = lapTime1;
  }

  public RaceTime getRaceTime()
  {
    return raceTime;
  }

  public void setRaceTime( final RaceTime raceTime1 )
  {
    raceTime = raceTime1;
  }

  public Ranking getRanking()
  {
    return ranking;
  }

  public void setRanking( final Ranking ranking1 )
  {
    ranking = ranking1;
  }

  public double getLapSeconds()
  {
    return lapTime == null ? 0 : lapTime.getMillis() / 1000.;
  }
}
