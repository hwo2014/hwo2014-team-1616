package msg;

public final class Race
{
  private Track       track;
  private CarData[]   cars;
  private RaceSession raceSession;

  public Track getTrack()
  {
    return track;
  }

  public void setTrack( final Track track1 )
  {
    track = track1;
  }

  public CarData[] getCars()
  {
    return cars;
  }

  public void setCars( final CarData[] cars1 )
  {
    cars = cars1;
  }

  public RaceSession getRaceSession()
  {
    return raceSession;
  }

  public void setRaceSession( final RaceSession raceSession1 )
  {
    raceSession = raceSession1;
  }
}
