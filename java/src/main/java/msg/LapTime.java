package msg;

public final class LapTime
{
  private int lap;
  private int ticks;
  private int millis;

  public int getLap()
  {
    return lap;
  }

  public void setLap( final int lap1 )
  {
    lap = lap1;
  }

  public int getTicks()
  {
    return ticks;
  }

  public void setTicks( final int ticks1 )
  {
    ticks = ticks1;
  }

  public int getMillis()
  {
    return millis;
  }

  public void setMillis( final int millis1 )
  {
    millis = millis1;
  }
}
