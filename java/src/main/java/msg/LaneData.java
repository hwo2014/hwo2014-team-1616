package msg;

@SuppressWarnings( "nls" )
public final class LaneData
{
  private int distanceFromCenter;
  private int index;

  public int getDistanceFromCenter()
  {
    return distanceFromCenter;
  }

  public void setDistanceFromCenter( final int distanceFromCenter1 )
  {
    distanceFromCenter = distanceFromCenter1;
  }

  public int getIndex()
  {
    return index;
  }

  public void setIndex( final int index1 )
  {
    index = index1;
  }

  @Override
  public String toString()
  {
    return "LaneData [distanceFromCenter=" + distanceFromCenter + ", index=" + index + "]";
  }
}
