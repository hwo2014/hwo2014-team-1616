package msg;

public final class Position
{
  private double x;
  private double y;

  public double getX()
  {
    return x;
  }

  public void setX( final double x1 )
  {
    x = x1;
  }

  public double getY()
  {
    return y;
  }

  public void setY( final double y1 )
  {
    y = y1;
  }
}
