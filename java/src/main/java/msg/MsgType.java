package msg;

import java.util.HashMap;
import java.util.Map;

import bot.U;

@SuppressWarnings( "nls" )
public enum MsgType
{
    JOIN( "join" )                          // <->
  , CREATE_RACE( "createRace" )             // <->
  , JOIN_RACE( "joinRace" )                 // <->

  , YOUR_CAR( "yourCar" )                   // <-
  , GAME_INIT( "gameInit" )                 // <-

    , GAME_START( "gameStart" )             // <-

      , CAR_POSITIONS( "carPositions" )     // <-
      , TURBO_AVAILABLE( "turboAvailable" ) // <-
      , TURBO( "turbo" )                    //  ->
      , TURBO_START( "turboStart" )         // <-
      , TURBO_END( "turboEnd" )             // <-
      , THROTTLE( "throttle" )              //  ->
      , SWITCH_LANE( "switchLane" )         //  ->
      , LAP_FINISHED( "lapFinished" )       // <-
      , CRASH( "crash" )                    // <-
      , SPAWN( "spawn" )                    // <-
      , FINISH( "finish" )                  // <-

    , GAME_END( "gameEnd" )                 // <-

  , TOURNAMENT_END( "tournamentEnd" )       // <-

  , FAILURE( "dnf" )                        // <-
  , PING( "ping" )                          //  ->
  ;

  private final static String              JSON_PREFIX = "{\"msgType\":\"";
  private final static Map<String,MsgType> typeMap;

  private final String                     type;
  private final String                     jsonPrefix;

  static
  {
    typeMap = new HashMap<>();

    for( final MsgType t : MsgType.values() )
      typeMap.put( t.type(), t );
  }

  MsgType( final String type1 )
  {
    type = type1;
    jsonPrefix = JSON_PREFIX + type + "\",";
  }

  public String type()
  {
    return type;
  }

  public boolean test( final String line )
  {
    return line != null && line.startsWith( jsonPrefix );
  }

  public static MsgType get( final String line )
  {
    if( line == null )
    {
      U.fail( "Empty message." );
      return null;
    }

    if( !line.startsWith( JSON_PREFIX ) )
    {
      U.fail( "Wrong message prefix: " + line );
      return null;
    }

    final int pos = line.indexOf( '"', JSON_PREFIX.length() );

    if( pos < 1 )
    {
      U.fail( "No matching \" for message type: " + line );
      return null;
    }

    final String type = line.substring( JSON_PREFIX.length(), pos );

    if( type == null || type.isEmpty() )
    {
      U.fail( "Empty message type: " + line );
      return null;
    }

    return typeMap.get( type );
  }
}
