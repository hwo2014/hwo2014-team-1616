package msg;

public final class PiecePosition
{
  private int    pieceIndex;
  private double inPieceDistance;
  private Lane   lane;
  private int    lap;

  public int getPieceIndex()
  {
    return pieceIndex;
  }

  public void setPieceIndex( final int pieceIndex1 )
  {
    pieceIndex = pieceIndex1;
  }

  public double getInPieceDistance()
  {
    return inPieceDistance;
  }

  public void setInPieceDistance( final double inPieceDistance1 )
  {
    inPieceDistance = inPieceDistance1;
  }

  public Lane getLane()
  {
    return lane;
  }

  public void setLane( final Lane lane1 )
  {
    lane = lane1;
  }

  public int getLap()
  {
    return lap;
  }

  public void setLap( final int lap1 )
  {
    lap = lap1;
  }
}
