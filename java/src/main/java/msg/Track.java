package msg;

import bot.U;

@SuppressWarnings( "nls" )
public final class Track
{
  private String        id;
  private String        name;
  private Piece[]       pieces;
  private LaneData[]    lanes;
  private StartingPoint startingPoint;

  public String getId()
  {
    return id;
  }

  public void setId( final String id1 )
  {
    id = id1;
  }

  public String getName()
  {
    return name;
  }

  public void setName( final String name1 )
  {
    name = name1;
  }

  public Piece[] getPieces()
  {
    return pieces;
  }

  public void setPieces( final Piece[] pieces1 )
  {
    pieces = pieces1;
  }

  public LaneData[] getLanes()
  {
    return lanes;
  }

  public void setLanes( final LaneData[] lanes1 )
  {
    lanes = lanes1;
  }

  public StartingPoint getStartingPoint()
  {
    return startingPoint;
  }

  public void setStartingPoint( final StartingPoint startingPoint1 )
  {
    startingPoint = startingPoint1;
  }

  @Override
  public String toString()
  {
    return String.format( "Track [id=%s, name=%s]\n\n%s\n\n%s", id, name, U.toString( pieces ), U.toString( lanes ) );
  }
}
